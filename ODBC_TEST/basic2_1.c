#include <windows.h>
#include <stdio.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
#include "basic.h"
#define MAX_DATA_LEN 1024
/* 检测返回代码是否为成功标志，当为成功标志返回 TRUE，否则返回 FALSE */
#define RC_SUCCESSFUL(rc) ((rc) == SQL_SUCCESS || (rc) == SQL_SUCCESS_WITH_INFO)
/* 检测返回代码是否为失败标志，当为失败标志返回 TRUE，否则返回 FALSE */
#define RC_NOTSUCCESSFUL(rc) (!(RC_SUCCESSFUL(rc)))
HENV henv; /* 环境句柄 */
HDBC hdbc; /* 连接句柄 */
HSTMT hsmt; /* 语句句柄 */
SQLRETURN sret;/* 返回代码 */

SQLRETURN   rc;
UCHAR errmsg[100];

void test_tmp_1(HENV henv, HDBC hdbc, HSTMT hsmt, SQLRETURN rc, int line, char filename[57]) {
	printf("%s  line %d \n", filename, line);
	if (rc != SQL_SUCCESS) {
		SQLError(&henv, &hdbc, hsmt, NULL, NULL, errmsg, sizeof(errmsg), NULL);
		printf("test_tmp_1 get error Because: %s！\n", errmsg);
		system("pause");
		exit(-1);
	}
	else {
		printf("test_tmp_1 get Ok！ just go on \n");
	}
}

void re_stmt_1(HSTMT hsmt) { //必须要指定为 HSTMT ，默认为 int 会 Error
	if (hsmt) {
		rc = SQLFreeHandle(SQL_HANDLE_STMT, hsmt);
		hsmt = NULL;
		//detect_fun("SQLFreeHandle", rc);
	}
	/* 申请一个语句句柄 */
	rc = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hsmt); //如果不释放后在申请，后面SQLTables 在用这个句柄时会报错，无效的游标状态 from http://bbs.csdn.net/topics/380206884 "odbc就是这点缺点，每次使用都要进行分配句柄"
}

void basic2_1(void)
{
	SQLCHAR szConnStrIn[256] = "";
	SQLCHAR szConnStrOut[256];
	SQLSMALLINT cbConnStrOut;
	/* 申请一个环境句柄 */

	rc = SQLAllocEnv(&henv);         /* allocate an environment handle   */
	rc = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
	rc = SQLAllocConnect(henv, &hdbc);  /* allocate a connection handle     */

	// MMMMM → "DRIVER=DM7 ODBC DRIVER" OK "DRIVER=DM ODBC DRIVER" Bad 要用这个名字去找驱动的！和 SQLDriverConnect 其他传入了 DSN 时的 DRIVER 是完全不一样的
	// 手册上是 DM ODBC DRIVER 为什么能够知道 要用 DM7 ODBC DRIVER ，去ODBC驱动管理界面里面 看名字就可以了

	rc = SQLBrowseConnect(hdbc, "DRIVER=DM7 ODBC DRIVER", SQL_NTS, szConnStrOut, 256, &cbConnStrOut);
	printf("rc is %d \n", rc);
	if (rc != SQL_NEED_DATA) {
		/* 连接数据源失败! */
		printf("…进行相应的错误处理…");
		exit(0);
	}

	// 放出来 直接看 define 对应什么值 //
	SQL_SUCCESS, SQL_SUCCESS_WITH_INFO, SQL_NEED_DATA, SQL_ERROR, SQL_INVALID_HANDLE, SQL_STILL_EXECUTING;

	//手册后面没引号，是不行的；通过写一个错误的IP地址就能够看出，报错的端口号是 错误的IP地址 和 523 的端口号
	rc = SQLBrowseConnect(hdbc, "SERVER=127.0.0.1;TCP_PORT=5236;", SQL_NTS, szConnStrOut, 256, &cbConnStrOut);
	printf("rc is %d \n", rc);
	if (rc != SQL_NEED_DATA) {
		/* 连接数据源失败! */
		printf("…进行相应的错误处理…");
		exit(0);
	}

	rc = SQLBrowseConnect(hdbc, "UID=SYSDBA;PWD=SYSDBA;", SQL_NTS, szConnStrOut, 256, &cbConnStrOut);
	detect_fun("SQLBrowseConnect", rc);

	printf("rc is %d \n", rc);
	if (rc != SQL_SUCCESS) {
		/* 连接数据源失败! */
		printf("…进行相应的错误处理…");
		exit(0);
	}
	char Info[20];
	int StringLength = 32;
	SQLPOINTER    InfoValuePtr = Info;
	SQLSMALLINT   BufferLength = 32;
	SQLSMALLINT   *FAR StringLengthPtr = &StringLength;
	rc = SQLGetInfo(hdbc,
		SQL_ACCESSIBLE_PROCEDURES,
		InfoValuePtr,
		BufferLength,
		StringLengthPtr);
	detect_fun("SQLGetInfo", rc);

	SQL_API_SQLALLOCCONNECT;
	/*
	SQLRETURN   SQLGetFunctions  (SQLHDBC           hdbc,
	SQLUSMALLINT      fFunction,
	SQLUSMALLINT FAR  *pfExists);
	*/
	int exitst = 0;
	rc = SQLGetFunctions(hdbc, SQL_API_SQLALLOCCONNECT, &exitst);
	detect_fun("SQLGetFunctions", rc);

	/*
	SQLGETDESCFIELD
	SQLGETDESCREC

	SQLSETDESCFIELD
	SQLSETDESCREC
	*/
	exitst = 0;	rc = SQLGetFunctions(hdbc, SQL_API_SQLGETDESCFIELD, &exitst);if(TRUE== exitst)detect_fun("SQLGETDESCFIELD", rc);
	exitst = 0;	rc = SQLGetFunctions(hdbc, SQL_API_SQLGETDESCREC, &exitst);	if (TRUE == exitst)detect_fun("SQLGETDESCREC", rc);
	exitst = 0;	rc = SQLGetFunctions(hdbc, SQL_API_SQLSETDESCFIELD, &exitst);	if (TRUE == exitst)detect_fun("SQLSETDESCFIELD", rc);
	exitst = 0;	rc = SQLGetFunctions(hdbc, SQL_API_SQLSETDESCREC, &exitst);	if (TRUE == exitst)detect_fun("SQLSETDESCREC", rc);
	printf("XX is %d", exitst); //system("pause");

	/*
	SQLRETURN   SQLNativeSql     (SQLHDBC           hdbc,
								   SQLCHAR      FAR  *szSqlStrIn,
								   SQLINTEGER        cbSqlStrIn,
								   SQLCHAR      FAR  *szSqlStr,
								   SQLINTEGER        cbSqlStrMax,
								   SQLINTEGER   FAR  *pcbSqlStr);
	*/
	char* sql_ = "select * from all_tables;";
	char* sql_out[200];
	int pcb_1;
	SQLCHAR      FAR  *szSqlStrIn = sql_;
	SQLCHAR      FAR  *szSqlStr = sql_out;
	SQLINTEGER   FAR  *pcbSqlStr = &pcb_1;
	rc = SQLNativeSql(hdbc, szSqlStrIn, strlen(sql_), szSqlStr, 200, pcbSqlStr);
	detect_fun("SQLNativeSql", rc);


	/* 申请一个语句句柄 */
	SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hsmt);

	/*
	SQLRETURN   SQLSetCursorName (SQLHSTMT          hstmt,
								   SQLCHAR     FAR   *szCursor,
								   SQLSMALLINT       cbCursor);
	*/
	SQLCHAR     FAR   *szCursor = "cur_name";
	SQLSMALLINT       cbCursor;
	rc = SQLSetCursorName(hsmt, szCursor, 32);
	detect_fun("SQLSetCursorName", rc);

	/*40.	SQLGetStmtAttr	返回语句属性值

		 SQLRETURN  SQLGetStmtAttr (SQLHSTMT          StatementHandle,
								SQLINTEGER        Attribute,
								SQLPOINTER        ValuePtr,
								SQLINTEGER        BufferLength,
								SQLINTEGER        *StringLengthPtr);
	*/
	SQLINTEGER sl = 32;
	SQLPOINTER        ValuePtr;
	SQLINTEGER        *StringLengthPtr1 = &sl;
	rc = SQLGetStmtAttr(hsmt, SQL_ATTR_CURSOR_TYPE, &ValuePtr, 0, StringLengthPtr1);
	detect_fun("SQLGetStmtAttr", rc);

	rc = SQLGetTypeInfo(hsmt, SQL_CHAR);//这里搞过之后，需要re_stmt才行
	detect_fun("SQLGetTypeInfo", rc);

	re_stmt_1(hsmt);//SQLGetTypeInfo之后，需要re_stmt才行

	/* 立即执行查询人员信息表的语句 */
	rc = SQLExecDirect(hsmt, (SQLCHAR *)"SELECT personid, name, phone FROM persion;", SQL_NTS);
	printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	rc = SQLCancel(hsmt); printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLCancel", rc);

	//create table EMPLOYEE(NAME varchar(20), ID int, PHOTO blob);
	SQLLEN cbNameParam, cbID = 0, cbData= MAX_DATA_LEN; //cbPhotoParam, cbData;
	char cbPhotoParam[4000];
	SWORD sID;
	PTR pToken, InitValue;
	UCHAR Data[MAX_DATA_LEN]= "publibfp.dhe.ibm.com/cgi-bin/bookmgr/BOOKS/dsnodk12/4.1.37?DT=20081113063437#HDRWQ2731";
	rc = SQLPrepare(hsmt,
		"INSERT INTO EMPLOYEE (NAME, ID, PHOTO) VALUES (?, ?, ?)",
		SQL_NTS);
	if (rc == SQL_SUCCESS) {
		/* Bind the parameters. For parameters 1 and 3, pass the */
		/* parameter number in rgbValue instead of a buffer address. */
		SQLBindParameter(hsmt, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR,
			20, 0, 1, 0, &cbNameParam);
		SQLBindParameter(hsmt, 2, SQL_PARAM_INPUT, SQL_C_SSHORT,
			SQL_SMALLINT, 0, 0, &sID, 0, &cbID);
		SQLBindParameter(hsmt, 3, SQL_PARAM_INPUT,
			SQL_C_BINARY, SQL_LONGVARBINARY,
			0, 0, 3, 0, &cbPhotoParam);
		rc = SQLExecute(hsmt); // 这里的 rc 需要是 99 NEED_MORE_DATA
		rc = SQLParamData(hsmt, &pToken); 
		detect_fun("SQLParamData", rc);
		rc = SQLPutData(hsmt, Data, cbData);// 字符串截断 create table EMPLOYEE(NAME varchar(2000), ID int, PHOTO blob); NAME 由 20 改成2000
		printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
		detect_fun("SQLPutData", rc);
	}

	/*
	SQLRETURN  SQLGetDiagRec  (SQLSMALLINT      HandleType,
                                SQLHANDLE        Handle,
                                SQLSMALLINT      RecNumber,
                                SQLCHAR          *SQLState,
                                SQLINTEGER       *NativeErrorPtr,
                                SQLCHAR          *MessageText,
                                SQLSMALLINT      BufferLength,
                                SQLSMALLINT      *TextLengthPtr);
	*/
	SQLCHAR          *SQLState=NULL;
		SQLINTEGER       *NativeErrorPtr= NULL;
		SQLCHAR          *MessageText= NULL;
		SQLSMALLINT      *TextLengthPtr= NULL;
	rc = SQLGetDiagRec(SQL_HANDLE_STMT, hsmt,1, SQLState, NativeErrorPtr, MessageText, 256,TextLengthPtr);
	if (SQL_NO_DATA == rc||SQL_SUCCESS==rc)rc = SQL_SUCCESS;
	detect_fun("SQLGetDiagRec", rc);
	SQL_SUCCESS; SQL_SUCCESS_WITH_INFO;
	//printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	rc = SQLGetDiagField(SQL_HANDLE_STMT, hsmt, 1, SQLState, NativeErrorPtr, MessageText, 256, TextLengthPtr);
	if (SQL_NO_DATA == rc || SQL_SUCCESS == rc)rc = SQL_SUCCESS;
	detect_fun("SQLGetDiagField", rc);
	re_stmt_1(hsmt); // look for http://publibfp.dhe.ibm.com/cgi-bin/bookmgr/BOOKS/dsnodk12/4.1.37?DT=20081113063437#HDRWQ2731

	//create table EMPLOYEE(NAME varchar(20), ID int, PHOTO blob);
	/*
	SQLRETURN   SQLParamData     (SQLHSTMT          hstmt,
	SQLPOINTER  FAR   *prgbValue);
	*/

	//system("pause");
	/* 释放语句句柄 */
	SQLFreeHandle(SQL_HANDLE_STMT, hsmt);
	/* 断开与数据源之间的连接 */
	rc = SQLDisconnect(hdbc);
	/* 释放连接句柄 */
	rc = SQLFreeConnect(hdbc);
	/* 释放环境句柄 */
	SQLFreeHandle(SQL_HANDLE_ENV, henv);
}