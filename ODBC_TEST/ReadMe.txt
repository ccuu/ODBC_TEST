1. 创建 LOCAL SYSDBA/SYSDBA 的本地 ODBC 源
2. 程序 用 64 位 ，odbc 要配置 64 的

接口列表：
序号	接口名	备注
1.	# SQLAllocConnect	用来分配连接句柄
2.	# SQLAllocEnv	用来分配环境句柄
3. 	# SQLAllocHandle	用来分配句柄
4.	# SQLAllocStmt	获取语句句柄
5.	# SQLBindCol	建立数据列和缓冲区中变量的关联
6.	# SQLBindParameter	在SQL语句中分配参数的缓冲区
7.	# SQLBrowseConnect	支持以一种迭代的方式获取到数据源的连接，直到最后建立连接
8.	# SQLBulkOperations	实现数据的更新
9.	# SQLCancel	取消现有的执行
10.	# SQLCloseCursor	关闭游标
11.	# SQLColAttribute	返回结果集中列的属性
12.	# SQLColumnPrivileges	返回一个关于指定表的列的列表以及相关的权限信息
13.	# SQLColumns	返回指定表的列信息的列表
14.	# SQLConnect	建立与驱动程序或者数据源的连接
15.	# SQLDescribeCol	返回结果集中列的描述符记录
16.	# SQLDescribeParam	返回对SQL语句中指定参数的描述
17.	# SQLDisconnect	关闭指定连接
18.	# SQLDriverConnect	与SQLConnect相似，用来连接到驱动程序或者数据源
19.	# SQLEndTran	提交或者回滚事务
20.	# SQLExecDirect	执行一条SQL语句
21.	# SQLExecute	执行准备好的SQL语句
22.	# SQLExtendedFetch	批量获取查询结果
23.	# SQLFetch	在结果集中检索下一行元组
24.	# SQLFetchScroll	返回指定的结果行
25.	# SQLForeignKeys	返回指定表的外键信息的列表
26.	# SQLFreeConnect	释放连接句柄
27.	# SQLFreeEnv	释放环境句柄
28.	# SQLFreeHandle	释放环境、连接、语句或者描述符句柄
29.	# SQLFreeStmt	释放语句句柄
30.	# SQLGetConnectAttr	返回连接属性值
31.	# SQLGetCursorName	返回与语句句柄相关的游标名称
32.	# SQLGetData	返回结果集中当前行某一列的值
33.	# # SQLGetDescField	返回单个描述符字段的值
34.	# # SQLGetDescRec	返回当前描述符记录的多个字段的值
35.	# SQLGetDiagField	返回一个字段值或者一个诊断数据记录
36.	# SQLGetDiagRec	返回多个字段值或者一个诊断数据记录
37.	# SQLGetEnvAttr	返回环境属性值
38.	# SQLGetFunctions	返回指定的驱动程序是否支持某个特定函数的信息
39.	# SQLGetInfo	返回连接的驱动程序和数据源的元信息
40.	# SQLGetStmtAttr	返回语句属性值
41.	# SQLGetTypeInfo	返回指定的数据源支持的数据类型的信息
42.	# SQLMoreResults	确定是否能够获得更多的结果集，如果能就执行下一个结果集的初始化操作
43.	# SQLNativeSql	返回驱动程序对一条SQL语句的翻译
44.	# SQLNumParams	返回SQL语句中参数的个数
45.	# SQLNumResultCols	返回结果集中列的数目
46.	# SQLParamData	与SQLPutData联合使用在运行时给参数赋值
47.	# SQLPrepare	准备要执行的SQL语句
48.	# SQLPrimaryKeys	返回指定表的主键信息的列表
49.	# SQLProcedureColumns	返回指定存储过程的参数信息的列表
50.	# SQLProcedures	返回指定数据源的存储过程信息的列表
51.	# SQLPutData	在SQL语句运行时给部分或者全部参数赋值
52.	# SQLRowCount	返回INSERT、UPDATE或者DELETE等语句影响的行数
53.	# SQLSetConnectAttr	设置连接属性值
54.	# SQLSetCursorName	设置与语句句柄相关的游标名称
55.	# # SQLSetDescField	设置单个描述符字段的值
56.	# # SQLSetDescRec	设置描述符记录的多个字段
57.	# SQLSetEnvAttr	设置环境属性值
58.	# SQLSetPos	在取到的数据集中设置游标的位置。这个记录集中的数据能够刷新、更新或者删除
59.	# SQLSetScrollOptions	设置控制游标行为的选项
60.	# SQLSetStmtAttr	设置语句属性值
61.	# SQLSpecialColumns	返回唯一确定某一行的列的信息，或者当某一事务修改一行的时候自动更新各列的信息
62.	# SQLStatistics	返回一个单表的相关统计信息和索引信息
63.	# SQLTablePrivileges	返回相关各表的名称以及相关的权限信息
64.	# SQLTables	返回指定数据源中表信息

绑定数据插入的例子：
http://publibfp.dhe.ibm.com/cgi-bin/bookmgr/BOOKS/dsnodk12/4.1.8?DT=20081113063437
msdn的odbc api文档：
https://technet.microsoft.com/en-us/library/ms130926(v=sql.110).aspx
一个比较 好/全 的 odbc 例子：
http://www.easysoft.com/developer/languages/c/examples/UsingParameterArraysAndKeySetCursors.html

IBM的ODBClibrary mostly gotten
http://publibfp.dhe.ibm.com/cgi-bin/bookmgr/BOOKS/dsnodk12/CCONTENTS

/*
这里每一个函数都具体调用了，另外一个可以考虑的简便方法是，  直接通过 SQLGetFunctions 这个函数去判断 

# # 的是用 SQLGetFunctions 验证的，在 basic2_1.c 文件中。
*/