
#include "externd.h"
HENV env;
HDBC dbc;
HSTMT stmt;
RETCODE ret;
short i;
int ss1;
short cols;
char colname[129];
char coldata[256];
SQLINTEGER attr;
SQLRETURN   rc = 0;
UCHAR errmsg[100];
char ff[500];
int si = 1;
FILE *fp_testodbc;

void detect_fun(char* ff, SQLRETURN rc) { // ocitest 中间的那个print错误，也是因为 ff 被默解析成int类型导致的
	//int i;
	//
	//__try
	//{
	//	i = rc;
	//}__except(1==1)
	//{
	//	printf("%s ---- fail！\n", ff);
	//	system("pause");
	//	exit(-1);
	//}
	if (rc == SQL_SUCCESS || (rc == SQL_NEED_DATA && "SQLParamData"==ff)) {
		printf("                                         %d %s ---- Pass！\n", si, ff);
		fprintf(fp_testodbc,"                                         %d %s ---- Pass！\n", si, ff);
		si++;
	}
	else {
		printf("                                         %s ---- fail！\n", ff);
		system("pause");
		exit(-1);
	}
}

void basic(void)
{
	rc = fopen_s(&fp_testodbc, "odbc_test.log", "w");

	int shouce = 2;

	attr = SQL_TRUE;

	if (1 == shouce)
	{//手册的连接方法
		SQLAllocHandle(SQL_HANDLE_ENV, NULL, &env);
		SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
		SQLAllocHandle(SQL_HANDLE_DBC, env, &dbc);

	}
	else {
		rc = SQLAllocEnv(&env);         /* allocate an environment handle   */
		detect_fun("SQLAllocEnv", rc);
		rc = SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
		detect_fun("SQLSetEnvAttr", rc);
		rc = SQLAllocConnect(env, &dbc);  /* allocate a connection handle     */
		detect_fun("SQLAllocConnect", rc);
	}
	ss1 = SQLConnect(dbc, (SQLCHAR *)"LOCAL", SQL_NTS, (SQLCHAR *)"SYSDBA", SQL_NTS, (SQLCHAR*)"SYSDBA", SQL_NTS);
	detect_fun("SQLConnect", ss1);



	if (ss1 != SQL_SUCCESS) { //连接失败必须退出，不然后面取结果集会有问题_空循环
		printf("数据源连接失败！\n");
		system("pause");
		exit(-1);
	}
	/* 设置连接句柄属性，关闭自动提交功能 */
	rc = SQLSetConnectAttr(dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)SQL_AUTOCOMMIT_OFF, SQL_IS_INTEGER); //默认是 SQL_AUTOCOMMIT_ON
	detect_fun("SQLSetConnectAttr", rc);
	/* 取得连接句柄属性，取得提交的模式 */
	rc = SQLGetConnectAttr(dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)&attr, sizeof(SQLINTEGER), NULL);
	detect_fun("SQLGetConnectAttr", rc);

	printf("-- %d --\n", attr);


	if (1 == shouce) {
		SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt);
	}
	else {
		rc = SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt);
		detect_fun("SQLAllocHandle", rc);
		rc = SQLAllocStmt(dbc, &stmt);// 异常写法 SQLAllocStmt(SQL_HANDLE_STMT,dbc, &stmt);
		detect_fun("SQLAllocStmt", rc);
	}

	rc = SQLExecDirect(stmt, (SQLCHAR *)"drop table test_table1;", SQL_NTS);
	
	rc = SQLExecDirect(stmt, (SQLCHAR *)"drop table persion;", SQL_NTS);
	rc = SQLExecDirect(stmt, (SQLCHAR *)"drop table EMPLOYEE;", SQL_NTS);
	
	SQLExecDirect(stmt, (SQLCHAR *)"drop table test_table2;", SQL_NTS);
	rc = SQLExecDirect(stmt, (SQLCHAR *)"create table test_table1 (t1col int);", SQL_NTS);
	detect_fun("SQLExecDirect", rc);
	SQLExecDirect(stmt, (SQLCHAR *)"create table persion (personid int, name varchar(256), phone number(12,2));", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"create table EMPLOYEE(NAME varchar(2000), ID int, PHOTO blob);", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"insert into persion values (121,'test_persion',100.00);", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"insert into test_table1 values(100);", SQL_NTS);
	rc = SQLEndTran(SQL_HANDLE_ENV, env, SQL_ROLLBACK); //回滚事务
	detect_fun("SQLEndTran", rc);
	SQLExecDirect(stmt, (SQLCHAR *)"insert into test_table1 values(200);", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"insert into persion values (122,'test_persion',100.30);", SQL_NTS);
	rc = SQLEndTran(SQL_HANDLE_ENV, env, SQL_COMMIT); //提交事务
	printf("rc %d \n", rc);
	SQLExecDirect(stmt, (SQLCHAR *)"create table test_table2 (t2col varchar(10));", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"insert into test_table2 values('hello!');", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"create or replace procedure test_proc as begin select * from test_table1;select * from test_table2;end;", SQL_NTS);
	SQLExecDirect(stmt, (SQLCHAR *)"call test_proc;", SQL_NTS);
	SQLNumResultCols(stmt, &cols);
	for (i = 1; i <= cols; i++)
	{
		SQLDescribeCol(stmt, i, (SQLCHAR *)colname, 129, NULL, NULL, NULL, NULL, NULL);
		printf("%s ", colname);
	}
	printf("\n");
	int tmp_1 = 0;
	for (; ; )
	{
		ret = SQLFetch(stmt);
		if (0 == tmp_1) {
			detect_fun("SQLFetch", ret);
			tmp_1++;
		}
		if (ret == SQL_NO_DATA_FOUND) break;
		for (i = 1; i <= cols; i++)
		{
			rc = SQLGetData(stmt, i, SQL_C_CHAR, coldata, 256, NULL);
			if (1 == tmp_1) {
				detect_fun("SQLGetData", rc);
				tmp_1++;
			}
			printf("%s ", coldata);
		}
		printf("\n");
	}
	rc = SQLMoreResults(stmt);
	detect_fun("SQLMoreResults", rc);
	rc = SQLNumResultCols(stmt, &cols);
	detect_fun("SQLNumResultCols", rc);
	for (i = 1; i <= cols; i++)
	{
		rc = SQLDescribeCol(stmt, i, (SQLCHAR *)colname, 129, NULL, NULL, NULL, NULL, NULL);
		if (2 == tmp_1) {
			detect_fun("SQLDescribeCol", rc);
			tmp_1++;
		}
		printf("%s ", colname);
	}
	printf("\n");
	for (; ; )
	{
		ret = SQLFetch(stmt);
		if (ret == SQL_NO_DATA_FOUND) break;
		for (i = 1; i <= cols; i++)
		{
			SQLGetData(stmt, i, SQL_C_CHAR, coldata, 256, NULL);
			printf("%s ", coldata);
		}
		printf("\n");
	}
	if (stmt) {
		rc = SQLFreeHandle(SQL_HANDLE_STMT, stmt);
		stmt = NULL;
		detect_fun("SQLFreeHandle", rc);
	}
	if (dbc) {
		rc = SQLDisconnect(dbc);
		//detect_fun("SQLDisconnect", rc); //这里释放会失败，应该要用 SQLFreeConnect 函数释放（见 basic2.c），如果用 SQLDisconnect 函数释放，连接应该是 通过 SQLDriverConnect 建立的
		rc = SQLFreeHandle(SQL_HANDLE_DBC, dbc);
		dbc = NULL;
	}
	if (env) {
		rc = SQLFreeHandle(SQL_HANDLE_ENV, env);
		env = NULL;
		//detect_fun("SQLFreeHandle", rc);
	}

	//rc = SQLFreeEnv(env);
	//detect_fun("SQLFreeEnv", rc);
	//system("pause");
}

