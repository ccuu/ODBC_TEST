#include <windows.h>
#include <stdio.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>

struct {
	SQLINTEGER ind;
	SQLCHAR str[29];
} tabcat, tabschem, tabname, tabtype;

void showtables(HSTMT hstmt) {
	int rc=0;
	strcpy_s((char *)tabschem.str,strlen("SYSDBA"),"SYSDBA");
	printf("\n SQLTables ");
	rc = SQLTables(hstmt, NULL, 0, tabschem.str, SQL_NTS, NULL, 0, NULL, 0);
	if (rc != SQL_SUCCESS) goto dberror;
	printf("\n SQLBindCol table name ");
	rc = SQLBindCol(hstmt, 3, SQL_C_CHAR, (SQLPOINTER)tabname.str, 128, &tabname.ind);
	if (rc != SQL_SUCCESS) {
		printf("\nSQLBindCol bind of table NAME failed");
		goto dberror;
	}
	rc = SQLBindCol(hstmt, 4, SQL_C_CHAR, (SQLPOINTER)tabtype.str, 128, &tabtype.ind);
	if (rc != SQL_SUCCESS) {
		printf("\n SQLBindCol bind of table type failed");
		goto dberror;
	}
	rc = SQLBindCol(hstmt, 2, SQL_C_CHAR, (SQLPOINTER)tabschem.str, 128, &tabschem.ind);
	if (rc != SQL_SUCCESS) {
		printf("\n SQLBindCol bind of table schem failed");
		goto dberror;
	}
	rc = SQLFetch(hstmt);
	while (rc == SQL_SUCCESS) {
		printf("\nDMLSEL1M SQLFetch tabschem=%s,tabname=%s,tabtype=%s",
			tabschem.str, tabname.str, tabtype.str);
		rc = SQLFetch(hstmt);
	}
dberror:
	printf("Im db error --- ");
	exit(-1);
}