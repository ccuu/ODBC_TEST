#include <windows.h>
#include <stdio.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
#include "basic.h"

//检测错误用 printf("rc is %d", rc);test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);

/* 检测返回代码是否为成功标志，当为成功标志返回 TRUE，否则返回 FALSE */
#define RC_SUCCESSFUL(rc) ((rc) == SQL_SUCCESS || (rc) == SQL_SUCCESS_WITH_INFO)
/* 检测返回代码是否为失败标志，当为失败标志返回 TRUE，否则返回 FALSE */
#define RC_NOTSUCCESSFUL(rc) (!(RC_SUCCESSFUL(rc)))
HENV henv; /* 环境句柄 */
HDBC hdbc; /* 连接句柄 */
HSTMT hsmt; /* 语句句柄 */
HENV henv1; /* 环境句柄 */
HDBC hdbc1; /* 连接句柄 */
HSTMT hsmt1; /* 语句句柄 */
SQLRETURN sret;/* 返回代码 */
int ss2;
char szpersonid[11]; /*人员编号*/
long cbpersonid = 0;
char szname[51]; /*人员姓名*/
long cbname = 0;
char szphone[26]; /*联系电话*/
long cbphone = 0;
SQLRETURN   rc;
UCHAR errmsg[100];
int tmp_test = 0;
SQLINTEGER  FAR   pcrow = 0; //Run-Time Check Failure #2 - Stack around the variable 'pcrow' was corrupted. 
//→ 是因为 pcrow 这个定义放在了 函数 basic2 里，在 函数 basic2 运行结束时，就会有上面那个错误
//→ 只要把 定义拿到外面就不会有这个报错
// 用到 pcrow 参数的函数是，SQLRETURN SQLRowCount (SQLHSTMT hstmt,SQLINTEGER  FAR   *pcrow); 
// 第二个参数的 Description : Pointer to location where the number of rows affected is stored.


void test_tmp(HENV henv, HDBC hdbc, HSTMT hsmt, SQLRETURN rc, int line, char filename[57]) {
	printf("%s  line %d \n", filename, line);
	if (rc != SQL_SUCCESS) {
		SQLError(&henv, &hdbc, hsmt, NULL, NULL, errmsg, sizeof(errmsg), NULL);
		printf("test_tmp get error Because: %s！\n", errmsg);
		system("pause");
		exit(-1);
	}
	else {
		printf("test_tmp get Ok！ just go on \n");
	}
}

void re_stmt(HSTMT hsmt) { //必须要指定为 HSTMT ，默认为 int 会 Error
	if (hsmt) {
		if (0 == tmp_test) {
			rc = SQLFreeStmt(hsmt, SQL_CLOSE); //第二个参数可以有这些 SQL_CLOSE SQL_DROP SQL_UNBIND SQL_RESET_PARAMS
			//printf("rc is %d", rc);
			detect_fun("SQLFreeStmt", rc);
			tmp_test++;
		}
		rc = SQLFreeHandle(SQL_HANDLE_STMT, hsmt);
		hsmt = NULL;
		//detect_fun("SQLFreeHandle", rc);
	}
	/* 申请一个语句句柄 */
	rc = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hsmt); //如果不释放后在申请，后面SQLTables 在用这个句柄时会报错，无效的游标状态 from http://bbs.csdn.net/topics/380206884 "odbc就是这点缺点，每次使用都要进行分配句柄"
}

void basic2(void)
{
	SQLCHAR szConnStrOut[256];
	SQLSMALLINT cbConnStrOut;
	SQLINTEGER output_nts;
	/* 申请一个环境句柄 */
	SQLAllocHandle(SQL_HANDLE_ENV, NULL, &henv);
	rc = SQLFreeEnv(henv);//printf("rc is %d", rc);
	detect_fun("SQLFreeEnv", rc);
	SQLAllocHandle(SQL_HANDLE_ENV, NULL, &henv);

	/* 设置环境句柄的 ODBC 版本 */
	SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
	/* 申请一个连接句柄 */
	SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);
	//ss2 = SQLConnect(hdbc, (SQLCHAR *)"LOCAL", SQL_NTS, (SQLCHAR *)"SYSDBA", SQL_NTS, (SQLCHAR *)"SYSDBA", SQL_NTS);
	//ss2 = SQLConnect(hdbc, (SQLCHAR *)"LOCAL", SQL_NTS, (SQLCHAR *)"SYSDBA", SQL_NTS, (SQLCHAR *)"SYSDBA", SQL_NTS);

	//SQLDriverConnect 第三个参数不能有空格
	ss2 = SQLDriverConnect(hdbc, NULL, (SQLCHAR*)"DSN=LOCAL;DRIVER=DM ODBC DRIVER;UID=SYSDBA;PWD=SYSDBA;TCP_PORT=5236;", SQL_NTS, szConnStrOut, 256, &cbConnStrOut, SQL_DRIVER_NOPROMPT);
	detect_fun("SQLDriverConnect", ss2);
	//system("pause");
	if (ss2 != SQL_SUCCESS) { //连接失败必须退出，不然后面取结果集会有问题_空循环
		printf("rc is %d \n", ss2);
		printf("数据源连接失败！\n");
		system("pause");
		exit(-1);
	}

	//rc = SQLFreeConnect(hdbc); printf("%d rc is %d\n", __LINE__, rc);
	//rc = SQLFreeEnv(henv); printf("%d rc is %d\n", __LINE__,rc);
	//rc = SQLAllocHandle(SQL_HANDLE_ENV, NULL, &henv); printf("%d rc is %d\n", __LINE__, rc);
	//rc = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc); printf("%d rc is %d\n", __LINE__, rc);

	//>> 可以改成释放申请的重置 函数，用if判断 释放函数放到后面
	//不释放，用新的句柄// /* 断开与数据源之间的连接 */
	//不释放，用新的句柄// rc = SQLDisconnect(hdbc); 
	//不释放，用新的句柄// 
	//不释放，用新的句柄// /* 释放连接句柄 */
	//不释放，用新的句柄// //rc = SQLFreeHandle(SQL_HANDLE_DBC, hdbc); //这个也可以，为了测试，用下面 odbc 1.0 的方法释放
	//不释放，用新的句柄// 
	//不释放，用新的句柄// rc = SQLFreeConnect(hdbc);hdbc = NULL;
	//不释放，用新的句柄// /* 释放环境句柄 */
	//不释放，用新的句柄// rc = SQLFreeHandle(SQL_HANDLE_ENV, henv); henv = NULL;

	//<< end


	rc = SQLGetEnvAttr(henv, SQL_ATTR_OUTPUT_NTS, &output_nts, 0, 0); //这个属性要连接后才有
	detect_fun("SQLGetEnvAttr", rc);

	/* 申请一个语句句柄 */
	SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hsmt);

	rc = SQLSetStmtAttr(hsmt, SQL_ATTR_CURSOR_TYPE, (SQLPOINTER)SQL_CURSOR_KEYSET_DRIVEN, 0);
	printf("rc is %d", rc);
	detect_fun("SQLSetStmtAttr", rc);


	//rc = SQLSetScrollOptions(hsmt, SQL_CONCUR_READ_ONLY, SQL_SCROLL_FORWARD_ONLY, 1);//这么设置后，后面就不能 SQLSetPos 了
	rc = SQLSetScrollOptions(hsmt, SQL_CONCUR_READ_ONLY, SQL_SCROLL_DYNAMIC, 1);//这么设置后，后面就不能
	//printf("rc is %d", rc);	test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLSetScrollOptions", rc);
	// 参考 http://download.oracle.com/otn_hosted_doc/timesten/703/TimesTen-Documentation/ms.odbc.pdf 555 页的部分
	//SQLSetScrollOptions 的调用，是需要在 SQLExecDirect 之前的

	/* 立即执行查询人员信息表的语句 */
	rc = SQLExecDirect(hsmt, (SQLCHAR *)"SELECT personid, name, phone FROM persion;", SQL_NTS);

	if (rc != SQL_SUCCESS) { //连接失败必须退出，不然后面取结果集会有问题_空循环
		SQLError(&henv, &hdbc, hsmt, NULL, NULL, errmsg, sizeof(errmsg), NULL);
		printf("语句 查询 失败 因为 %s！\n", errmsg);
		system("pause");
		exit(-1);
	}

	//create table persion (personid varchar(11), name varchar(51), phone varchar(26));
	// insert into persion valurs ('121','test_persion','13000001000');
	/* 绑定数据缓冲区 */
	rc = SQLBindCol(hsmt, 1, SQL_C_CHAR, szpersonid, sizeof(szpersonid), &cbpersonid);
	detect_fun("SQLBindCol", rc);
	SQLBindCol(hsmt, 2, SQL_C_CHAR, szname, sizeof(szname), &cbname);
	SQLBindCol(hsmt, 3, SQL_C_CHAR, szphone, sizeof(szphone), &cbphone);
	/* 取得数据并且打印数据 */
	printf("编号 文本 数值\n");

	/*
	SQLRETURN SQLBulkOperations (
 |                SQLHSTMT      StatementHandle,
 |                SQLSMALLINT   Operation);
	*/

	rc = SQLBulkOperations(hsmt, SQL_ADD); printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLBulkOperations", rc);

	int tmp_1 = 0;
	for (;;) {
		rc = SQLFetchScroll(hsmt, SQL_FETCH_NEXT, 0);
		if (0 == tmp_1) {
			detect_fun("SQLFetchScroll", rc);
			tmp_1++;
		}

		if (1 == tmp_1) { //SQLSetPos放到这个if前面，就会重复输出，死循环了
			rc = SQLSetPos(
				hsmt,
				1,                   /* Position at the first row of the rowset. */
				SQL_POSITION,
				SQL_LOCK_NO_CHANGE); /* Do not change the lock state. */
			detect_fun("SQLSetPos", rc);
			tmp_1++;
		}
		if (rc == SQL_NO_DATA_FOUND)
			break;
		printf("%s %s %s\n", szpersonid, szname, szphone);
	}
	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLTables(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*)  "PERSION", SQL_NTS, NULL, 0);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLTables", rc);
	
	//re_stmt(hsmt);
	//char aa[20] = "test"; SQL_SUCCESS, SQL_SUCCESS_WITH_INFO, SQL_ERROR, SQL_INVALID_HANDLE;
	//rc = SQLSetDescField(hsmt,1,1,aa,20); printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);

	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLColumns(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "SYSDBA", SQL_NTS, NULL, 0);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLColumns", rc);

	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLColumnPrivileges(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "SYSDBA", SQL_NTS, NULL, 0);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLColumnPrivileges", rc);

	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLProcedures(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "TEST_PROC", SQL_NTS);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLProcedures", rc);



	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLSpecialColumns(hsmt, SQL_BEST_ROWID, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS,
		(SQLCHAR*)  "PERSION", SQL_NTS, SQL_SCOPE_CURROW, SQL_NULLABLE);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLSpecialColumns", rc);


	//测试 SQLExtendedFetch Start → 
	//重新申请句柄 测试 		rc = SQLExtendedFetch(hsmt, SQL_FETCH_NEXT, 0, &pcrow, Row_Stat);
	//printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	re_stmt(hsmt);
	rc = SQLExecDirect(hsmt, (SQLCHAR *)"SELECT personid, name FROM persion;", SQL_NTS);

	SQLPOINTER       CharacterAttributePtr = NULL;
	SQLSMALLINT      BufferLength = 3;
	SQLSMALLINT      *StringLengthPtr = NULL;
	SQLPOINTER       NumericAttributePtr = NULL;
	rc = SQLColAttribute(hsmt, 1, SQL_DESC_BASE_COLUMN_NAME, CharacterAttributePtr, BufferLength, StringLengthPtr, NumericAttributePtr);
	//printf("rc is %d", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLColAttribute", rc);

	if (rc != SQL_SUCCESS) { //连接失败必须退出，不然后面取结果集会有问题_空循环
		SQLError(&henv, &hdbc, hsmt, NULL, NULL, errmsg, sizeof(errmsg), NULL);
		printf("语句 查询 失败 因为 %s！\n", errmsg);
		system("pause");
		exit(-1);
	}

	//create table persion (personid int, name varchar(256), phone number(12,2))
	// insert into persion valurs ('121','test_persion','13000001000');
	/* 绑定数据缓冲区 */
	rc = SQLBindCol(hsmt, 1, SQL_C_CHAR, szpersonid, sizeof(szpersonid), &cbpersonid);
	SQLBindCol(hsmt, 2, SQL_C_CHAR, szname, sizeof(szname), &cbname);
	int i = 0;
	int Row_Stat[1];
	rc = -8;
	int pr = 0;
	while ((rc = SQLExtendedFetch(hsmt, SQL_FETCH_NEXT, 0, &pcrow, Row_Stat)) == SQL_SUCCESS) {
		if (0 == pr) { detect_fun("SQLExtendedFetch", rc); pr++; }// 通过 pr 避免重复输出
		for (i = 0; i < pcrow; i++) {
			printf("--这里可以把数据输出出来--这里不输出了");
		}
		if (pcrow < 1)
			break;
	}
	//← 测试 SQLExtendedFetch end
	//system("pause");

	SQLSMALLINT pcb = 0;
	char cb[20];
	SQLCHAR     FAR   *szCursor = cb;
	SQLSMALLINT       cbCursorMax = 32;
	SQLSMALLINT FAR   *pcbCursor = pcb;
	rc = SQLGetCursorName(hsmt,
		szCursor,
		cbCursorMax,
		pcbCursor);// *pcbCursor 是不对的，包括 *szCursor 用 szCursor 和 pcbCursor
	detect_fun("SQLGetCursorName", rc);

	rc = SQLCloseCursor(hsmt);
	detect_fun("SQLCloseCursor", rc);


	re_stmt(hsmt);
	rc = SQLProcedureColumns(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "TEST_PROC", SQL_NTS, NULL, 0);
	detect_fun("SQLProcedureColumns", rc);

	re_stmt(hsmt);
	rc = SQLForeignKeys(hsmt,
		NULL,
		0,
		(SQLCHAR *) "SYSDBA",
		SQL_NTS,
		(SQLCHAR *) "PERSONID",
		SQL_NTS,
		NULL,
		0,
		NULL,
		SQL_NTS,
		NULL,
		SQL_NTS);
	detect_fun("SQLForeignKeys", rc);

	re_stmt(hsmt);
	rc = SQLPrimaryKeys(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "TEST_PROC", SQL_NTS);
	detect_fun("SQLPrimaryKeys", rc);


	re_stmt(hsmt);
	//showtables(hsmt);
	rc = SQLTablePrivileges(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*) "SYSDBA", SQL_NTS);
	/*--0000-- 这里可以把信息输出出来，这里不输出了*/
	//test_tmp(henv, hdbc, hsmt,rc,__LINE__,__FILE__);
	detect_fun("SQLTablePrivileges", rc);

	re_stmt(hsmt);
	rc = SQLStatistics(hsmt, NULL, 0, (SQLCHAR*) "SYSDBA", SQL_NTS, (SQLCHAR*)  "PERSION", SQL_NTS, SQL_INDEX_UNIQUE, SQL_QUICK);
	//test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLStatistics", rc);

	re_stmt(hsmt);
	SQLCHAR         stmt_sql[] =
		"INSERT INTO PERSION VALUES (?, ?, ?)";
	SQLINTEGER      Prod_Num[NUM_PRODS] = {
		100110, 100120, 100210, 100220, 100510, 100520, 200110,
		200120, 200210, 200220, 200510, 200610, 990110, 990120,
		500110, 500210, 300100
	};
	SQLCHAR         Description[NUM_PRODS][257] = {
		"Aquarium-Glass-25 litres", "Aquarium-Glass-50 litres",
		"Aquarium-Acrylic-25 litres", "Aquarium-Acrylic-50 litres",
		"Aquarium-Stand-Small", "Aquarium-Stand-Large",
		"Pump-Basic-25 litre", "Pump-Basic-50 litre",
		"Pump-Deluxe-25 litre", "Pump-Deluxe-50 litre",
		"Pump-Filter-(for Basic Pump)",
		"Pump-Filter-(for Deluxe Pump)",
		"Aquarium-Kit-Small", "Aquarium-Kit-Large",
		"Gravel-Colored", "Fish-Food-Deluxe-Bulk",
		"Plastic-Tubing"
	};
	SQLDOUBLE       UPrice[NUM_PRODS] = {
		110.00, 190.00, 100.00, 150.00, 60.00, 90.00, 30.00,
		45.00, 55.00, 75.00, 4.75, 5.25, 160.00, 240.00,
		2.50, 35.00, 5.50
	};
	SQLUINTEGER     pirow = 0;

	SQLSMALLINT FAR     *pfSqlType = NULL;
	SQLUINTEGER FAR     *pcbColDef = NULL;
	SQLSMALLINT FAR     *pibScale = NULL;
	SQLSMALLINT FAR     *pfNullable = NULL;

	/* Prepare the statement */
	rc = SQLPrepare(hsmt, stmt_sql, SQL_NTS); printf("rc is %d\n", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLPrepare", rc);

	rc = SQLDescribeParam(hsmt,
		1,
		pfSqlType,
		pcbColDef,
		pibScale,
		pfNullable); printf("rc is %d\n", rc); test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLDescribeParam", rc);
	//system("pause");

	rc = SQLNumParams(hsmt, pcrow);
	detect_fun("SQLNumParams", rc);

	rc = SQLBindParameter(hsmt, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER,
		0, 0, Prod_Num, 0, NULL); printf("rc is %d\n", rc);
	detect_fun("SQLBindParameter", rc);

	rc = SQLBindParameter(hsmt, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR,
		257, 0, Description, 257, NULL); printf("rc is %d\n", rc);
	rc = SQLBindParameter(hsmt, 3, SQL_PARAM_INPUT, SQL_C_DOUBLE, SQL_DECIMAL,
		10, 2, UPrice, 0, NULL); printf("rc is %d\n", rc);
	rc = SQLParamOptions(hsmt, NUM_PRODS, pirow); printf("rc is %d\n", rc); //这个就尴尬了，这里不能用 &pirow 要用 pirow
	detect_fun("++SQLParamOptions++", rc);
	rc = SQLExecute(hsmt); printf("rc is %d\n", rc);
	detect_fun("SQLExecute", rc);
	printf("Inserted end ... \n");
	/* ... */

	rc = SQLRowCount(hsmt, &pcrow);
	printf("rc is %d,rows is %d \n", rc, pcrow);

	detect_fun("SQLRowCount", rc);

	/* 释放语句句柄 */
	SQLFreeHandle(SQL_HANDLE_STMT, hsmt);
	/* 断开与数据源之间的连接 */
	rc = SQLDisconnect(hdbc);
	detect_fun("SQLDisconnect", rc);

	/* 释放连接句柄 */
	//rc = SQLFreeHandle(SQL_HANDLE_DBC, hdbc); //这个也可以，为了测试，用下面 odbc 1.0 的方法释放

	rc = SQLFreeConnect(hdbc);
	test_tmp(henv, hdbc, hsmt, rc, __LINE__, __FILE__);
	detect_fun("SQLFreeConnect", rc);

	/* 释放环境句柄 */
	SQLFreeHandle(SQL_HANDLE_ENV, henv);
}